

import org.springframework.beans.factory.support.PropertiesBeanDefinitionReader;
import org.springframework.context.support.GenericApplicationContext;
import someSimpleBeans.Quest;
import someSimpleBeans.SomeQuest;

public class PropertyFileApplicationContext extends GenericApplicationContext {
    public PropertyFileApplicationContext(String fileName) {
        PropertiesBeanDefinitionReader reader = new PropertiesBeanDefinitionReader(this);
        reader.loadBeanDefinitions(fileName);
        refresh();
    }


    public static void main(String[] args) {
        PropertyFileApplicationContext context = new PropertyFileApplicationContext("context.properties");
        SomeQuest sq = (SomeQuest) context.getBean(Quest.class);
        sq.getQuest();
    }

}